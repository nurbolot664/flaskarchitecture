import logging

from flask import Flask

from src.api.controller.register import bind_routes
from src.api.exceptions.register import register_exception_handler
from src.api.register_depends import register_dependency_injection


def initialize_app() -> Flask:
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    _app = Flask(__name__)
    bind_routes(_app)
    register_exception_handler(_app)
    register_dependency_injection(_app)
    return _app


def main():
    app = initialize_app()
    app.run(debug=True, port=5001)


if __name__ == '__main__':
    main()
