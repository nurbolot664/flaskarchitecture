from typing import Protocol


class IUnitOfWork(Protocol):
    def commit(self) -> None:
        raise NotImplementedError

    def flush(self) -> None:
        raise NotImplementedError

    def rollback(self) -> None:
        raise NotImplementedError
