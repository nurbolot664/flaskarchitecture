import os

from dotenv import load_dotenv
from pydantic.v1 import BaseSettings
load_dotenv()


class Settings(BaseSettings):
    POSTGRES_URI: str = os.environ.get('POSTGRES_URI')

    SECRET_KEY: str = os.environ.get('SECRET_KEY')
    EXPIRE_MINUTES: int = os.environ.get('EXPIRE_MINUTES', default=5)
    ALGORITHM: str = os.environ.get('ALGORITHM')
    REDIS_URI: str = os.environ.get('REDIS_URI')
    # REFRESH_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 7
    # REFRESH_TOKEN_EXPIRE_MINUTES: float = os.environ.get('REFRESH_TOKEN_EXPIRE_MINUTES', default=5)


settings = Settings()
