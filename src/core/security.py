from datetime import (
    datetime,
    timedelta,
)
from typing import Final, Union, Any
from uuid import UUID

from jose import (
    ExpiredSignatureError,
    JWTError,
    jwt,
)
from jose.exceptions import JWTClaimsError
from passlib.context import CryptContext

from src.core.settings import settings

pwd_context: Final = CryptContext(
    schemes=['bcrypt'], deprecated='auto',
)


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


def hashing_secret(secret: str) -> str:
    return pwd_context.hash(secret=secret)


def create_jwt_token(user_id: UUID, scopes: list | None):
    claims = dict(
        sub=user_id.__str__(),
        scopes=scopes,
        exp=datetime.utcnow() + timedelta(minutes=settings.EXPIRE_MINUTES)
    )
    return jwt.encode(
        claims=claims, key=settings.SECRET_KEY, algorithm=settings.ALGORITHM
    )


def decode_jwt_token(jwt_token: str) -> dict:
    try:
        payload = jwt.decode(
            jwt_token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
        )
        user_id = payload.get("sub")
        if user_id is None:
            raise ValueError('Invalid token')

    except (JWTError, JWTClaimsError, ExpiredSignatureError) as e:
        raise ValueError(e)

    return user_id


ACCESS_TOKEN_EXPIRE_MINUTES = 30  # 30 minutes
REFRESH_TOKEN_EXPIRE_MINUTES = 60 * 24 * 7 # 7 days
JWT_SECRET_KEY = "narscbjim@$@&^@&%^&RFghgjvbdsha"   # should be kept secret
JWT_REFRESH_SECRET_KEY = "13ugfdfgh@#$%^@&jkl45678902"
ALGORITHM = "HS256"


def create_refresh_token(subject: Union[str, Any], expires_delta: int = None) -> str:
    if expires_delta is not None:
        expires_delta = datetime.utcnow() + expires_delta
    else:
        expires_delta = datetime.utcnow() + timedelta(minutes=REFRESH_TOKEN_EXPIRE_MINUTES)

    to_encode = {"exp": expires_delta, "sub": str(subject)}
    encoded_jwt = jwt.encode(to_encode, JWT_REFRESH_SECRET_KEY, ALGORITHM)

    return encoded_jwt
