from sqlalchemy import (
    create_engine,
    orm,
)

from src.core.settings import settings


def sync_session(url: str) -> orm.scoped_session:
    engine = create_engine(
        url, pool_pre_ping=True, future=True,
    )
    factory = orm.sessionmaker(
        engine, autoflush=False, expire_on_commit=False,
    )
    return orm.scoped_session(factory)


sync_session_impl = sync_session(settings.POSTGRES_URI.replace('+asyncpg', ''))
