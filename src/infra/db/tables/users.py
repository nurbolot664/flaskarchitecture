import sqlalchemy as sa

from src.infra.db.tables.common import Base


class User(Base):
    __tablename__ = "users"

    username = sa.Column(
        sa.String,
        unique=True,
        nullable=False,
    )
    email = sa.Column(
        sa.String,
        unique=True,
        nullable=False,
    )
    password = sa.Column(
        sa.String(255),
        nullable=False,
    )
