from functools import wraps
from typing import Any, Callable

from sqlalchemy.exc import IntegrityError


def exception_mapper(func: Callable[..., Any]) -> Callable[..., Any]:
    @wraps(func)
    def wrapped(*args: Any, **kwargs: Any):
        try:
            return func(*args, **kwargs)
        except IntegrityError as err:
            raise Exception from err

    return wrapped
