from select import select
from uuid import UUID

from injector import inject
from sqlalchemy.orm import Session

from src.infra.db.exception_mapper import exception_mapper
from src.infra.db.repositories.common import UnitOfWork
from src.infra.db.tables.users import User
from src.interfaces.alchemy.users import IUsersRepository


class UserRepository(UnitOfWork, IUsersRepository):
    @inject
    def __init__(self, session: Session) -> None:
        # Call parent class constructor with session
        super().__init__(session)

    @exception_mapper
    def insert_user(self, instance: User):
        self.session.add(instance=instance)

    @exception_mapper
    def get_user_by_email(self, email: str) -> User:
        user = self.session.query(User).filter(User.email == email).one()
        return user

    def get_user_by_id(self, user_id: UUID) -> User:
        return self.session.query(User).filter(User.id == user_id).one()
