from injector import inject
from sqlalchemy.orm import Session

from src.core.uow import IUnitOfWork


class SQLAlchemyBaseUoW(IUnitOfWork):
    @inject
    def __init__(self, session: Session):
        self._session = session

    def commit(self) -> None:
        self._session.commit()

    def rollback(self) -> None:
        self._session.rollback()


class UnitOfWork(IUnitOfWork):
    def __init__(self, session: Session) -> None:
        self.session = session
