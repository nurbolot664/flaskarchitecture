from src.api.controller.auth import auth
from src.api.controller.health import health


def bind_routes(_app):
    _app.register_blueprint(health)
    _app.register_blueprint(auth)
