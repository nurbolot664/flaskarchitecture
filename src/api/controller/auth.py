from flask import request, Blueprint
from injector import inject

from src.services.auth.service import (
    RegisterService,
    UserRegisterRequest,
    AuthorizationService,
    UserLoginRequest,
    UserProfileService,
    TokenRequest,
    RefreshTokenService,
)

auth = Blueprint('user', __name__)


@auth.route('/register', methods=['POST'])
@inject
def create_user(service: RegisterService):
    return service.register_user(dto=UserRegisterRequest(**request.json))


@auth.route('/login', methods=['POST'])
@inject
def login(service: AuthorizationService):
    return service.authenticate(dto=UserLoginRequest(**request.json))


@auth.route('/refresh_token', methods=['POST'])
@inject
def refresh_token(service: RefreshTokenService):
    return service.new_access_token(dto=TokenRequest(**request.json))


@auth.route('/profile', methods=['POST'])
@inject
def user_profile(service: UserProfileService):
    return service.profile(dto=TokenRequest(**request.json))
