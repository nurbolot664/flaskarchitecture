from flask import jsonify
from sqlalchemy.exc import IntegrityError


def already_exists_handler(error):
    if isinstance(error, IntegrityError):
        return jsonify({'error': 'User with this email already exists', 'code': 409})
    return jsonify({'error': 'Internal Server Error'}), 500
