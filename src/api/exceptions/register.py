from sqlalchemy.exc import IntegrityError

from src.api.exceptions.handler import already_exists_handler
# from src.services.exceptions import AlreadyExistsError


def register_exception_handler(_app):
    # _app.register_error_handler(AlreadyExistsError, already_exists_handler)
    _app.register_error_handler(IntegrityError, already_exists_handler)

    return _app
