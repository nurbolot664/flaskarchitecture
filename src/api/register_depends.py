from flask_injector import FlaskInjector
from injector import Binder, singleton
from sqlalchemy.orm import Session

from src.core.database import sync_session_impl
from src.core.uow import IUnitOfWork
from src.infra.db.repositories.common import SQLAlchemyBaseUoW
from src.infra.db.repositories.users import UserRepository
from src.interfaces.alchemy.users import IUsersRepository

from src.api.exceptions.handler import already_exists_handler
from src.services.auth.service import AuthorizationService
# from src.services.exceptions import AlreadyExistsError


def configure(binder: Binder) -> Binder:
    binder.bind(IUsersRepository, to=UserRepository, scope=singleton)
    binder.bind(IUnitOfWork, to=SQLAlchemyBaseUoW, scope=singleton)
    binder.bind(Session, to=sync_session_impl, scope=singleton)

    binder.bind(AuthorizationService, scope=singleton)

    # binder.bind(AlreadyExistsError, to=already_exists_handler)

    return binder


def register_dependency_injection(app):
    FlaskInjector(app=app, modules=[configure])
