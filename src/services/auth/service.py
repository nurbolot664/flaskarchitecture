import logging
from multiprocessing import AuthenticationError

from flask import jsonify, Response
from injector import inject
from pydantic import BaseModel

from src.core import security
from src.core.security import hashing_secret, verify_password
from src.core.uow import IUnitOfWork
from src.infra.db.tables.users import User
from src.interfaces.alchemy.users import IUsersRepository

logger = logging.getLogger(__name__)


class UserRegisterRequest(BaseModel):
    username: str
    email: str
    password: str


class UserLoginRequest(BaseModel):
    email: str
    password: str


class TokenRequest(BaseModel):
    token: str


class UserProfileResponse(BaseModel):
    username: str
    email: str


class RegisterService:

    @inject
    def __init__(
            self,
            repo: IUsersRepository,
            uow: IUnitOfWork,
    ) -> None:
        self.repo = repo
        self.uow = uow

    def register_user(self, dto: UserRegisterRequest):
        hashed_password = hashing_secret(dto.password)
        user = User(
            username=dto.username,
            email=dto.email,
            password=hashed_password
        )
        self.repo.insert_user(user)
        self.uow.commit()
        logger.info("User created: %s,", dto.username)
        return jsonify({'message': 'User created successfully', 'username': dto.username}), 201


class AuthorizationService:

    @inject
    def __init__(
            self,
            repo: IUsersRepository,
            uow: IUnitOfWork,
    ) -> None:
        self.repo = repo
        self.uow = uow

    def authenticate_user(self, email: str, password: str) -> None | User:
        user = self.repo.get_user_by_email(email)

        if user and verify_password(password, user.password):
            return user

    def authenticate(self, dto: UserLoginRequest):
        if not (
                user := self.authenticate_user(email=dto.email, password=dto.password)
        ):
            raise AuthenticationError
        access_token = security.create_jwt_token(user_id=user.id, scopes=None)
        refresh_token = security.create_refresh_token(subject=str(user.id))
        logger.info("User has logged: id=%s, %s", user.id, user.email)
        return {"access_token": access_token, "refresh_token": refresh_token}


class UserProfileService:

    @inject
    def __init__(
            self,
            repo: IUsersRepository,
            uow: IUnitOfWork,
    ) -> None:
        self.repo = repo
        self.uow = uow

    def profile(self, dto: TokenRequest):
        user_id = security.decode_jwt_token(dto.token)
        user = self.repo.get_user_by_id(user_id=user_id)
        return UserProfileResponse(username=user.username, email=user.email).dict()


class RefreshTokenService:

    @inject
    def __init__(
            self,
            repo: IUsersRepository,
            uow: IUnitOfWork,
    ) -> None:
        self.repo = repo
        self.uow = uow

    def new_access_token(self, dto: TokenRequest):
        access_token = security.create_refresh_token(subject=dto.token)
        print(access_token)
        logger.info("User received a refresh token:", access_token)
        return TokenRequest(token=access_token).dict()
