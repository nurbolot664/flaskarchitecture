from abc import (
    ABC,
    abstractmethod,
)

from uuid import UUID

from src.infra.db.tables.users import User


class IUsersRepository(ABC):

    @abstractmethod
    def insert_user(self, instance: User):
        raise NotImplementedError

    @abstractmethod
    def get_user_by_email(self, username: str):
        raise NotImplementedError

    @abstractmethod
    def get_user_by_id(self, user_id: UUID):
        raise NotImplementedError
